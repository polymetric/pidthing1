final float frictionCoefficient = 0.9;
final float framerate = 60;

long time = 0;
float target;

VoltageControlledMotor motor;
PIDController pid;

void setup() {
  size(800, 800);
  smooth(8);
  frameRate(framerate);
  
  target = 6.28;
  
  motor = new VoltageControlledMotor(width/2, height/2, 300, 190);
  motor.voltage = 0;
  
  pid = new PIDController();
  
  pid.gainP = 0.6;
  pid.gainI = 0;
  pid.gainD = 0.1;
  
  //pid.gainP = 0.8;
  //pid.gainI = 5.33;
  //pid.gainD = 0.0792;
  
  pid.limit = 22;
}

void draw() {
  background(32);
  
  //target = map(mouseY, 0, height, 0, 6.28);
  target = -atan2(mouseX-width/2, mouseY-height/2);
  
  float error = target - motor.angle;
  motor.voltage = pid.operator(error);
  //motor.voltage = map(mouseY, 0, height, 1, 0);
  
  motor.tick();
  motor.draw();
  
  System.out.printf("time: %12d error: %12.6f volt: %12.6f vel: %12.6f \n", time, error, motor.voltage, motor.vel * (0.5/PI) * framerate * 60);
  time++;
}

void keyPressed() {
  switch (key) {
    case 'o':
      target += PI;
      break;
    case 'r':
      setup();
      break;
  }
}

float addDeadzone(float x, float dz) {
  if (x >= 0) {
    return max(x-dz, 0);
  } else {
    return min(x+dz, 0);
  }
}
