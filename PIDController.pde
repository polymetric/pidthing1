class PIDController {
  float gainP = 0;
  float gainI = 0;
  float gainD = 0;
  float integral_prev = 0;
  float error_prev = 0;
  long time_prev = 0;
  float limit = 0;
  
  public PIDController() {
    
  }
  
  float operator(float error) {
    long now = System.nanoTime();
    float Ts = (now - time_prev) * 1e-9;
    if(Ts <= 0 || Ts > 0.5) Ts = 1e-3;
    
    float p = gainP * error;
    
    float i = integral_prev + gainI * Ts * 0.5 * (error + error_prev);
    i = constrain(i, -limit, limit);
    
    float d = gainD * (error - error_prev) / Ts;
    
    float output = p + i + d;
    output = constrain(output, -limit, limit);
    
    integral_prev = i;
    error_prev = error;
    time_prev = now;
    return output;
  }
}
