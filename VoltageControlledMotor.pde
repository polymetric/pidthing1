class VoltageControlledMotor {
  PVector pos;
  float radius;
  float angle;
  float vel;
  float strength;
  float voltage;
  
  public VoltageControlledMotor(float x, float y, float radius, float strength) {
    pos = new PVector(x, y);
    vel = 0;
    this.radius = radius;
    this.strength = strength;
  }
  
  public void tick() {
    angle += vel;
    vel *= frictionCoefficient;
    vel += voltage * strength * (1.0/framerate) * (1.0/60) * (0.5/PI) / 0.25;
  }
  
  public void draw() {
    stroke(224);
    strokeWeight(10);
    
    PVector a, b, c;
    a = pos.copy();
    b = pos.copy();
    c = new PVector(0, radius);
    c.rotate(angle);
    a.add(c);
    b.sub(c);
    line(a.x, a.y, b.x, b.y);
  }
}
